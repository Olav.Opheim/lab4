package datastructure;

import javax.print.attribute.standard.CopiesSupported;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int cols; // Width
    private int rows; // Hight
    private CellState[][] grid; // Grid [cols] [rows]
    //private CellState initialState;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows; // Assigns value to rows
        this.cols = columns; // Assigns value to columns
        this.grid =  new CellState[this.rows][this.cols]; // creates a new grid[][] object
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.grid[row][col] = initialState; //this double for loop initializes the states for each cell
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows; // Returns the height fo the rows
    }

    @Override
    public int numColumns() {
        return this.cols; // Returns the width of the columns
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element; // Sets the state in the cell position given
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column]; // reutrns the cell position in the given cell.
    }    

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(this.rows, this.cols, null); // cellular.CellState.DEAD
        for (int row = 0; row < this.rows; row++) { // double for loop that assigns value and states for every cell
            for (int col = 0; col < this.cols; col++) {
                gridCopy.set(row, col, this.get(row, col));
            }
        }
        return gridCopy; // returns the gridcopy
    }   
}
